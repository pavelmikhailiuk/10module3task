package by.epam.task.orm.init.impl;

import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.sql.ResultSet;
import java.sql.SQLException;

import by.epam.task.orm.init.EntityInitializer;

/**
 * Created by Pavel on 9/11/2016.
 */
public class EntityInitializerImpl implements EntityInitializer {
    @Override
    public Object init(Class<?> clazz, ResultSet resultSet) throws IllegalAccessException, InstantiationException, SQLException {
        Object obj=clazz.newInstance();
        for (Field field : clazz.getDeclaredFields()) {
            field.setAccessible(true);
            String name = field.getName();
            String type= field.getType().getTypeName();
           switch (type){
               case "java.lang.String": field.set(obj,resultSet.getString(name));
                   break;
               case "java.lang.Long" : field.set(obj,resultSet.getLong(name));
                   break;
           }
        }
        return obj;
    }
}
