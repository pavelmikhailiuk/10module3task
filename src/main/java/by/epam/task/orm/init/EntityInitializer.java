package by.epam.task.orm.init;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Pavel on 9/11/2016.
 */
public interface EntityInitializer {
    Object init(Class<?> clazz, ResultSet resultSet) throws IllegalAccessException, InstantiationException, SQLException;
}
