package by.epam.task.orm.manager;

import java.util.List;

/**
 * Created by Pavel on 9/10/2016.
 */
public interface EntityManager {
    Long save(Object entity);
    Object find(Class<?> clazz, Long id);
    void update(Object entity);
    void delete(Class<?> clazz, Long id);
}
