package by.epam.task.orm.manager.impl;

import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

import org.apache.log4j.Logger;

import by.epam.task.orm.builder.QueryBuilder;
import by.epam.task.orm.datasource.DataSource;
import by.epam.task.orm.exception.OrmException;
import by.epam.task.orm.init.EntityInitializer;
import by.epam.task.orm.manager.EntityManager;

/**
 * Created by Pavel on 9/10/2016.
 */
public class EntityManagerImpl implements EntityManager {
	private final static Logger LOGGER = Logger.getLogger(EntityManager.class);
	private DataSource dataSource;
	private EntityInitializer initializer;
	private Map<String, Map<String, String>> allQueries;

	public EntityManagerImpl(DataSource dataSource, QueryBuilder builder, EntityInitializer initializer) {
		this.dataSource = dataSource;
		this.initializer = initializer;
		this.allQueries = builder.getAllQueries();
	}

	@Override
	public Long save(Object entity) {
		Connection conn = dataSource.getConnection();
		Long id = null;
		try {
			Map<String, String> query = allQueries.get(entity.getClass().getSimpleName());
			LOGGER.info(query.get("save"));
			PreparedStatement statement = conn.prepareStatement(query.get("save"));
			initPreparedStatement(statement, entity, false);
			statement.execute();
			ResultSet tableKeys = statement.getGeneratedKeys();
			tableKeys.next();
			id = tableKeys.getLong(1);
		} catch (SQLException | IllegalArgumentException | IllegalAccessException e) {
			LOGGER.error(e);
			throw new OrmException("Can't save entity");
		} finally {
			dataSource.putConnection(conn);
		}
		return id;
	}

	@Override
	public Object find(Class<?> clazz, Long id) {
		Connection conn = dataSource.getConnection();
		Object object = null;
		try {
			Map<String, String> query = allQueries.get(clazz.getSimpleName());
			LOGGER.info(query.get("find"));
			PreparedStatement statement = conn.prepareStatement(query.get("find"), 1);
			statement.setLong(1, id);
			statement.execute();
			ResultSet resultSet = statement.getResultSet();
			object = initializer.init(clazz, resultSet);
		} catch (IllegalAccessException e) {
			LOGGER.error(e);
			throw new OrmException("Can't access entity fields");
		} catch (SQLException e) {
			LOGGER.error(e);
			throw new OrmException("Can't find or init entity");
		} catch (InstantiationException e) {
			LOGGER.error(e);
			throw new OrmException("Can't instantiate entity");
		} finally {
			dataSource.putConnection(conn);
		}
		return object;
	}

	@Override
	public void update(Object entity) {
		Connection conn = dataSource.getConnection();
		try {
			Map<String, String> query = allQueries.get(entity.getClass().getSimpleName());
			LOGGER.info(query.get("update"));
			PreparedStatement statement = conn.prepareStatement(query.get("update"));
			initPreparedStatement(statement, entity, true);
			statement.execute();
		} catch (SQLException | IllegalArgumentException | IllegalAccessException e) {
			LOGGER.error(e);
			throw new OrmException("Can't update entity");
		} finally {
			dataSource.putConnection(conn);
		}
	}

	@Override
	public void delete(Class<?> clazz, Long id) {
		Connection conn = dataSource.getConnection();
		try {
			Map<String, String> query = allQueries.get(clazz.getSimpleName());
			LOGGER.info(query.get("delete"));
			PreparedStatement statement = conn.prepareStatement(query.get("delete"), 1);
			statement.setLong(1, id);
			statement.execute();
		} catch (SQLException e) {
			LOGGER.error(e);
			throw new OrmException("Can't delete entity");
		} finally {
			dataSource.putConnection(conn);
		}
	}

	private void initPreparedStatement(PreparedStatement preparedStatement, Object entity, boolean update)
			throws IllegalArgumentException, IllegalAccessException, SQLException {
		Long id = null;
		int i = 1;
		for (Field field : entity.getClass().getDeclaredFields()) {
			field.setAccessible(true);
			String fieldName = field.getName();
			if (fieldName.equalsIgnoreCase("id")) {
				id = (Long) field.get(entity);
				continue;
			}
			preparedStatement.setObject(i, field.get(entity));
			i++;
		}
		if (update) {
			preparedStatement.setLong(i, id);
		}
	}
}
