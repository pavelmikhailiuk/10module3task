package by.epam.task.orm.util;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

public class ClassFinder {
	private final static Logger LOGGER = Logger.getLogger(ClassFinder.class);

	private static final char PACKAGE_SEPARATOR = '.';

	private static final char DIR_SEPARATOR = '/';

	private static final String EXTENSION = ".class";

	public static List<Class<?>> find(String pkg) {
		String scannedPath = pkg.replace(PACKAGE_SEPARATOR, DIR_SEPARATOR);
		URL scannedUrl = Thread.currentThread().getContextClassLoader().getResource(scannedPath);
		if (scannedUrl == null) {
			LOGGER.error("Error in provided path: " + scannedPath);
			throw new IllegalArgumentException("Error in provided path");
		}
		File scannedDir = new File(scannedUrl.getFile());
		List<Class<?>> classes = new ArrayList<>();
		for (File file : scannedDir.listFiles()) {
			classes.addAll(find(file, pkg));
		}
		return classes;
	}

	private static List<Class<?>> find(File file, String pkg) {
		List<Class<?>> classes = new ArrayList<>();
		String resource = pkg + PACKAGE_SEPARATOR + file.getName();
		if (file.isDirectory()) {
			for (File child : file.listFiles()) {
				classes.addAll(find(child, resource));
			}
		} else if (resource.endsWith(EXTENSION)) {
			int endIndex = resource.length() - EXTENSION.length();
			String className = resource.substring(0, endIndex);
			try {
				classes.add(Class.forName(className));
			} catch (ClassNotFoundException e) {
				LOGGER.info("Class not found in provided package " + pkg, e);
			}
		}
		return classes;
	}
}