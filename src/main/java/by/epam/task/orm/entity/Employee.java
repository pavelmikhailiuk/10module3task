package by.epam.task.orm.entity;

/**
 * Created by Pavel on 9/10/2016.
 */
public class Employee {
	private Long id;
	private String name;
	private String surname;
	private Long deptId;

	public Employee() {
	}

	public Employee(Long id, String name, String surname, Long deptId) {
		this.id = id;
		this.name = name;
		this.surname = surname;
		this.deptId = deptId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public Long getDeptId() {
		return deptId;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		Employee empoyee = (Employee) o;

		if (id != null ? !id.equals(empoyee.id) : empoyee.id != null)
			return false;
		if (name != null ? !name.equals(empoyee.name) : empoyee.name != null)
			return false;
		if (surname != null ? !surname.equals(empoyee.surname) : empoyee.surname != null)
			return false;
		return deptId != null ? deptId.equals(empoyee.deptId) : empoyee.deptId == null;

	}

	@Override
	public int hashCode() {
		int result = id != null ? id.hashCode() : 0;
		result = 31 * result + (name != null ? name.hashCode() : 0);
		result = 31 * result + (surname != null ? surname.hashCode() : 0);
		result = 31 * result + (deptId != null ? deptId.hashCode() : 0);
		return result;
	}

	public void setDeptId(Long deptId) {
		this.deptId = deptId;
	}

	@Override
	public String toString() {
		return "Empoyee{" + "id=" + id + ", name='" + name + '\'' + ", surname='" + surname + '\'' + ", deptId="
				+ deptId + '}';
	}
}
