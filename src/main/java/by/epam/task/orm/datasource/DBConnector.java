package by.epam.task.orm.datasource;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ResourceBundle;

/**
 * Created by Pavel on 9/10/2016.
 */
public class DBConnector {
	private static ResourceBundle dbConfig = ResourceBundle.getBundle("db");

	public static Connection getConnection() throws SQLException {
		Connection connection = DriverManager.getConnection(dbConfig.getString("url"), dbConfig.getString("user"),
				dbConfig.getString("password"));
		return connection;
	}
}
