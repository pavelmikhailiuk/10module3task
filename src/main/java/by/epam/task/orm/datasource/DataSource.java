package by.epam.task.orm.datasource;

import java.sql.Connection;

/**
 * Created by Pavel on 9/10/2016.
 */
public interface DataSource {
    Connection getConnection();

    void putConnection(Connection connection);

    void cleanUp();
}
