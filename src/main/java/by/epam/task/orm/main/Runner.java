package by.epam.task.orm.main;

import by.epam.task.orm.builder.QueryBuilder;
import by.epam.task.orm.builder.impl.QueryBuilderImpl;
import by.epam.task.orm.datasource.DataSource;
import by.epam.task.orm.datasource.impl.ConnectionPool;
import by.epam.task.orm.entity.Department;
import by.epam.task.orm.entity.Employee;
import by.epam.task.orm.init.EntityInitializer;
import by.epam.task.orm.init.impl.EntityInitializerImpl;
import by.epam.task.orm.manager.EntityManager;
import by.epam.task.orm.manager.impl.EntityManagerImpl;

/**
 * Created by Pavel on 9/10/2016.
 */
public class Runner {
	public static void main(String[] args) {
		DataSource dataSource = ConnectionPool.getInstance();
		QueryBuilder builder = new QueryBuilderImpl();
		EntityInitializer initializer = new EntityInitializerImpl();
		EntityManager entityManager = new EntityManagerImpl(dataSource, builder, initializer);
		Department department = new Department();
		department.setName("CDP");
		Long deptId = entityManager.save(department);
		Employee employee = new Employee();
		employee.setName("Jonh");
		employee.setSurname("Doe");
		employee.setDeptId(deptId);
		Long empId = entityManager.save(employee);
		employee = (Employee) entityManager.find(Employee.class, empId);
		employee.setSurname("Smith");
		entityManager.update(employee);
		entityManager.delete(Employee.class, empId);
	}
}
