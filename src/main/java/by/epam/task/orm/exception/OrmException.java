package by.epam.task.orm.exception;

/**
 * Created by Pavel on 9/10/2016.
 */
public class OrmException extends RuntimeException {
    public OrmException() {
    }

    public OrmException(String message) {
        super(message);
    }

    public OrmException(String message, Throwable cause) {
        super(message, cause);
    }

    public OrmException(Throwable cause) {
        super(cause);
    }
}
