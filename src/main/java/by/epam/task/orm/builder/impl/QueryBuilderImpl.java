package by.epam.task.orm.builder.impl;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import by.epam.task.orm.annotations.Column;
import by.epam.task.orm.annotations.Table;
import by.epam.task.orm.builder.QueryBuilder;
import by.epam.task.orm.util.ClassFinder;

/**
 * Created by Pavel on 9/10/2016.
 */
public class QueryBuilderImpl implements QueryBuilder {

	private static ResourceBundle dbConfig = ResourceBundle.getBundle("db");

	private Map<String, Map<String, String>> allQueries = new HashMap<>();

	public QueryBuilderImpl() {
		init();
	}

	@Override
	public Map<String, Map<String, String>> getAllQueries() {
		return allQueries;
	}

	private void init() {
		List<Class<?>> classes = ClassFinder.find(dbConfig.getString("scan.package"));
		for (Class clazz : classes) {
			String className = clazz.getSimpleName();
			Map<String, String> actionQueries = allQueries.get(className);
			if (actionQueries == null) {
				actionQueries = new HashMap<>();
				buildSequenceQuery(actionQueries, className);
				buildTriggerQuery(actionQueries, className);
				buildDeleteQuery(actionQueries, className);
				buildFindQuery(actionQueries, className);
				buildSaveUpdateQuery(actionQueries, clazz);
				allQueries.put(className, actionQueries);
			}
		}
	}

	private void buildSequenceQuery(Map<String, String> actionQueries, String className) {
		actionQueries.put("sequence", "CREATE SEQUENCE \"" + className.toUpperCase()
				+ "_SEQ\"  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 21");
	}

	private void buildTriggerQuery(Map<String, String> actionQueries, String className) {
		actionQueries.put("trigger",
				"CREATE OR REPLACE TRIGGER \"" + className.toUpperCase()
						+ "_ID_TRG\" before insert on DEPARTMENT for each row begin  if :new.\"id\" is null then select "
						+ className.toUpperCase() + "_seq.nextval into :new.\"id\" from dual; end if; end;");
	}

	private void buildDeleteQuery(Map<String, String> actionQueries, String className) {
		actionQueries.put("delete", "delete from " + className + " where id= ?");
	}

	private void buildFindQuery(Map<String, String> actionQueries, String className) {
		actionQueries.put("find", "select from " + className + " where id= ?");
	}

	private void buildSaveUpdateQuery(Map<String, String> actionQueries, Class clazz) {
		String tableName = clazz.getSimpleName();
		Table classAnnotation = (Table) clazz.getAnnotation(Table.class);
		if (classAnnotation != null) {
			tableName = classAnnotation.name();
		}
		StringBuilder insertQuery = new StringBuilder();
		insertQuery.append("insert into ").append(tableName).append(" ");
		StringBuilder tableFields = new StringBuilder();
		tableFields.append("(");
		StringBuilder tableValues = new StringBuilder();
		tableValues.append("(");
		StringBuilder updateQuery = new StringBuilder();
		updateQuery.append("update ").append(tableName);
		for (Field field : clazz.getDeclaredFields()) {
			field.setAccessible(true);
			String columnName = field.getName();
			Column fieldAnnotation = field.getAnnotation(Column.class);
			if (fieldAnnotation != null) {
				columnName = fieldAnnotation.name();
			}
			if (!columnName.equalsIgnoreCase("id")) {
				tableFields.append("\"").append(columnName).append("\",");
				tableValues.append("?,");
				updateQuery.append(" set \"").append(field).append("\"= ?,");
			}
		}
		insertQuery.append(removeLastComma(tableFields.toString())).append(") values ")
				.append(removeLastComma(tableValues.toString())).append(")");
		actionQueries.put("update", removeLastComma(updateQuery.toString()) + " where id= ?");
		actionQueries.put("save", insertQuery.toString());
	}

	private String removeLastComma(String str) {
		return str.substring(0, str.length() - 1);
	}
}
