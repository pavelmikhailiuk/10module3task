package by.epam.task.orm.builder;

import java.util.Map;

/**
 * Created by Pavel on 9/10/2016.
 */
public interface QueryBuilder {
	Map<String, Map<String, String>> getAllQueries();
}
